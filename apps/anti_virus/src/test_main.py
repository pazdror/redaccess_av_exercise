import threading
from unittest.mock import patch

import main
import requests
from main import app, fetch_words, is_threat_detected, words


def test_process_string():
    app.testing = True
    with app.test_client() as client:
        input_string = 'This is a test string'
        response = client.post('/api/check_string', data=input_string.encode('utf-8'))
        assert response.status_code == 200
        assert response.get_data(as_text=True) == 'clean'

def test_is_threat_detected():
    words = ['test', 'malicious']
    with patch.object(main, 'words', words):
        assert is_threat_detected('This is a malicious test') is True
        assert is_threat_detected('This is a safe string') is False

def test_fetch_words(monkeypatch):
    mock_response = {'status_code': 200, 'json': lambda: ['test', 'malicious']}
    def mock_get(*args, **kwargs):
        return MockResponse(mock_response)
    monkeypatch.setattr(requests, 'get', mock_get)

    fetch_words()
    assert getattr(main, 'words', []) == ['test', 'malicious']

class MockResponse:
    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response['json']()

    @property
    def status_code(self):
        return self.response['status_code']


if __name__ == '__main__':
    threading.Thread(target=lambda: app.run(), daemon=True).start()
    threading.Thread(target=fetch_words , daemon=True).start()
