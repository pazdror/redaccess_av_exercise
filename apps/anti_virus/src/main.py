import logging
import threading
from time import sleep
import requests
from flask import Flask, request

CHECK_RESULTS = ['detected', 'clean']
WORDS_REFRESH_INTERVAL = 60 # SECONDS
WORDS_HANDLER_URL = 'http://words-service/api/get_words'

words: list[str] = []

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
app = Flask(__name__)


def is_threat_detected(string: str) -> bool:
    '''Check for malicious words in string'''
    print('words', words)
    return any((i in string for i in words))


@app.route('/api/check_string', methods=['POST'])
def process_string():
    '''Validate a string is safe to pass'''
    input_string = request.get_data().decode('utf-8')
    # process the input string here
    string_clean = not is_threat_detected(input_string)
    logger.info(f"Processed string <{input_string}>: {CHECK_RESULTS[string_clean]}")
    return CHECK_RESULTS[string_clean]


def fetch_words():
    '''Fetch and update list of malicious words'''
    response = requests.get(WORDS_HANDLER_URL, timeout=30)
    if response.status_code != 200:
        raise ConnectionError(f'Error fetching words ({response.status_code}): {response.json()}')
    data = response.json()
    global words #  pylint: disable=global-statement
    words = data.copy()
    logger.debug("Fetched words list")

def fetch_words_task():
    '''Run `fetch_words` continuously'''
    while True:
        fetch_words()
        sleep(WORDS_REFRESH_INTERVAL)


if __name__ == '__main__':
    threading.Thread(target=lambda: app.run(), daemon=True).start()
    threading.Thread(target=fetch_words_task() , daemon=True)
