import logging
import os
import pathlib

from flask import Flask, request

WORDS_FILE = pathlib.Path(os.environ.get('WORDS_FILE', '/tmp/data/words.txt'))
WORDS_REFRESH_INTERVAL = 60 # SECONDS
DEFAULT_WORDS = {"eval('unescape.", "eicar"}
words: set[str] = set()

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
app = Flask(__name__)


def read_words_file() -> set[str]:
    try:
        return set(WORDS_FILE.read_text(encoding='UTF-8').splitlines())
    except (FileNotFoundError, PermissionError):
        logging.warning("could not read words from file. Using default words")
        return DEFAULT_WORDS


def write_words_file(words_to_write: set[str]):
    try:
        words_file = pathlib.Path(WORDS_FILE)
        words_file.parent.mkdir(parents=True, exist_ok=True)
        words_file.write_text('\n'.join(words_to_write), encoding='UTF-8')
    except PermissionError:
        logger.error('Permission error writing words file.')


def load_words():
    '''Load malicious words list'''
    global words #  pylint: disable=global-statement
    words = read_words_file()
    logger.info(f'Loaded {len(words)} malicious words')


@app.post("/api/add_word")
def update_words():
    '''Add new word to malicious words list'''
    new_word = request.get_data().decode('utf-8')
    words.add(new_word)
    write_words_file(words)
    logger.info(f'Added <{new_word}> to malicious words list')
    return 'OK', 200


@app.get('/api/get_words')
def get_words():
    '''Send malicious words list to client'''
    if not words:
        logger.error('Words list is empty')
        return 'No words in list', 500
    logger.info(f'{len(words)} words sent to client')
    return list(words), 200

if __name__ == '__main__':
    load_words()
    app.run()
