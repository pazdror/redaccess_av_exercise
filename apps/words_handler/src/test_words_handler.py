import pathlib
from unittest.mock import patch

import pytest
from requests.exceptions import ConnectionError

import words_handler
from words_handler import app, load_words, WORDS_FILE, WORDS_REFRESH_INTERVAL, DEFAULT_WORDS


TEST_WORDS = {'foo', 'bar'}
@pytest.fixture(scope='session')
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


def test_get_words(client):
    with patch.object(words_handler, 'words', TEST_WORDS):
        response = client.get('/api/get_words')
        assert response.status_code == 200
        assert not TEST_WORDS.symmetric_difference(response.json)


def test_get_words_words_is_empty(client):
    with patch.object(words_handler, 'words', set()):
        response = client.get('/api/get_words')
        assert response.status_code == 500
        assert response.data == b'No words in list'


def test_update_words(client):
    with patch.object(pathlib.Path, 'write_text') as write_text_mock:
        with patch('words_handler.load_words'):
            response = client.post('/api/add_word', data='baz')
            assert response.status_code == 200
            assert response.data == b'OK'
            write_text_mock.assert_called_with('baz', encoding='UTF-8')


def test_load_words():
    with patch.object(pathlib.Path, 'read_text') as read_text_mock:
        read_text_mock.return_value = 'foo\nbar\n'
        load_words()
        assert TEST_WORDS == set(getattr(words_handler, 'words', set()))


def test_load_words_file_not_found():
    with patch.object(pathlib.Path, 'read_text', side_effect=FileNotFoundError):
        load_words()
        print('WORDS ',getattr(words_handler, 'words', set()))
        assert DEFAULT_WORDS == set(getattr(words_handler, 'words', set()))


def test_load_words_file_empty():
    with patch.object(pathlib.Path, 'read_text') as read_text_mock:
        read_text_mock.return_value = ''
        load_words()
        assert set() == set(getattr(words_handler, 'words', set()))
