#Red Access Anti Virus Exercise

## Build docker images

```
docker build --pull --rm -f "apps\anti_virus\Dockerfile" -t ra-antivirus "apps\anti_virus"
docker build --pull --rm -f "apps\words_handler\Dockerfile" -t ra-words "apps\words_handler"
```

## Set persistent storage
Windows example:
`mkdir c:\tmp\ra_data`

For different location or OS, please adjust file `k8s/volums.yaml`, field `spec.local.path`

## Deploy K8S cluster

`kubectl apply -f .\k8s`

## Query apps
Scan text:
`curl -X POST -d "string to scan" localhost:30031/api/check_string`

Get malicious words list:
`curl -X GET localhost:30030/api/get_words`

Add a malicious word to list
`curl -X POST -d "Word to add" localhost:30030/api/add_word`